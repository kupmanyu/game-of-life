#include <stdio.h>
#include "src/pgmIO.h"
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define IMHT 64
#define IMWD 64

void DataInStream(unsigned char pic[IMHT][IMWD], char infname[100])
{
  int res;
  unsigned char line[ IMWD ];
  //char infname[] = "64x64.pgm";     //put your input image path here
  printf( "DataInStream: Start...\n" );

  //Open PGM file
  res = _openinpgm( infname, IMWD, IMHT );
  if( res ) {
    printf( "DataInStream: Error openening %s\n.", infname );
    return;
  }

  //Read image line-by-line and send byte by byte to channel c_out
  for( int y = 0; y < IMHT; y++ ) {
    _readinline( line, IMWD );
    for( int x = 0; x < IMWD; x++ ) {
      pic[y][x] = line[x];
      //printf( "-%4.1d ", line[ x ] ); //show image values
    }
    //printf( "\n" );
  }

  //Close PGM image file
  _closeinpgm();
  printf( "DataInStream: Done...\n" );
  return;
}

void DataOutStream(unsigned char pic[IMHT][IMWD], char outfname[100]){
  int res;
  unsigned char line[ IMWD ];
  //char outfname[] = "64x64correct.pgm"; //put your output image path here
  //int output = 0;
      //outon :> output;
      //Open PGM file
      //printf( "DataOutStream: Start...\n" );
      res = _openoutpgm( outfname, IMWD, IMHT );
      if( res ) {
        printf( "DataOutStream: Error opening %s\n.", outfname );
        return;
      }

      //Compile each line of the image and write the image line-by-line
      for( int y = 0; y < IMHT; y++ ) {
        for (int x = 0; x < IMWD; x++) {
          line[x] = pic[y][x];
        }
        _writeoutline( line, IMWD );
        //printf( "DataOutStream: Line written...\n" );
      }

      //Close the PGM image
      _closeoutpgm();
      printf( "DataOutStream: Done...\n" );
}

void askRounds(char text[100]) {
  printf("Number of rounds: ");
  fgets(text, 100, stdin);
}

void add(int live[IMHT][IMWD], int x, int y) {
  int directions[8][2] = {{0, 1}, {1, 1}, {1, 0}, {1, (IMHT-1)}, {0, (IMHT-1)}, {(IMWD-1), (IMHT-1)}, {(IMWD-1), 0}, {(IMWD-1), 1}};
  int modx, mody;
  for (int i = 0; i < 8; i++) {
    modx = (x + directions[i][0]) % IMWD;
    mody = (y + directions[i][1]) % IMHT;
    live[mody][modx] += 1;
  }
}

void liveadd(unsigned char pic[IMHT][IMWD], int live[IMHT][IMWD]) {
  for (int y = 0; y < IMHT; y++) {
    for (int x = 0; x < IMWD; x++) {
      if (pic[y][x] == 255) add(live, x, y);
    }
  }
}

void game_of_life(unsigned char pic[IMHT][IMWD], int live[IMHT][IMWD]) {
  unsigned char currentcell;
  int livecell;
  for (int y = 0; y < IMHT; y++) {
    for (int x = 0; x < IMWD; x++) {
      currentcell = pic[y][x];
      livecell = live[y][x];
      if ((currentcell == 255) && (livecell < 2)) pic[y][x] = 0;
      else if ((currentcell == 255) && ((livecell == 2) || (livecell == 3))) pic[y][x] = 255;
      else if ((currentcell == 255) && (livecell > 3)) pic[y][x] = 0;
      else if ((currentcell == 0) && (livecell == 3)) pic[y][x] = 255;
      else pic[y][x] = 0;
      live[y][x] = 0;
    }
  }
}

int main() {
    unsigned char pic[IMHT][IMWD];
    int live[IMHT][IMWD];
    char inname[100];
    char outname[100];
    char temp[10];
    snprintf(temp, 10, "%d", IMWD);
    strcpy(inname, temp);
    strcat(inname, "x");
    snprintf(temp, 10, "%d", IMHT);
    strcat(inname, temp);
    strcpy(outname, inname);
    strcat(inname, ".pgm");
    DataInStream(pic, inname);
    char line[100];
    askRounds(line);
    int rounds = atoi(line);
    for (int i = 0; i < rounds; i++) {
      liveadd(pic, live);
      game_of_life(pic, live);
    }
    strcat(outname, "correct");
    snprintf(temp, 10, "%d", rounds);
    strcat(outname, temp);
    strcat(outname, ".pgm");
    DataOutStream(pic, outname);
    return 0;
}
